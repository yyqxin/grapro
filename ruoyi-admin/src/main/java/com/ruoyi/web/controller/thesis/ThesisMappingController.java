package com.ruoyi.web.controller.thesis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.SysUser;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.thesis.domain.ThesisMapping;
import com.ruoyi.thesis.service.IThesisMappingService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 学生选题Controller
 * 
 * @author grapro
 * @date 2020-08-17
 */
@Controller
@RequestMapping("/thesis/mapping")
public class ThesisMappingController extends BaseController
{
    private String prefix = "thesis/mapping";

    @Autowired
    private IThesisMappingService thesisMappingService;

    @RequiresPermissions("thesis:mapping:view")
    @GetMapping()
    public String mapping()
    {
        return prefix + "/mapping";
    }

    /**
     * 查询学生选题列表
     */
    @RequiresPermissions("thesis:mapping:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ThesisMapping thesisMapping)
    {
        startPage();
        // 学生自建题目需要限制学生只看到自己的题目，教师能看到自己论文的
        SysUser currentUser = ShiroUtils.getSysUser();
        String roleKeys = currentUser.getRoleKeys();
        if (roleKeys.contains("teacher")){
            Map<String,Object> map = new HashMap<>();
            map.put("dsf"," and t.teacher_id = '"+ShiroUtils.getUserId()+"' ");
            thesisMapping.setParams(map);
        }else if (roleKeys.contains("student")){
            Map<String,Object> map = new HashMap<>();
            map.put("dsf"," and t.student_id = '"+ShiroUtils.getUserId()+"' ");
            thesisMapping.setParams(map);
        }
        List<ThesisMapping> list = thesisMappingService.selectThesisMappingList(thesisMapping);
        return getDataTable(list);
    }

    /**
     * 导出学生选题列表
     */
    @RequiresPermissions("thesis:mapping:export")
    @Log(title = "学生选题", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ThesisMapping thesisMapping)
    {
        List<ThesisMapping> list = thesisMappingService.selectThesisMappingList(thesisMapping);
        ExcelUtil<ThesisMapping> util = new ExcelUtil<ThesisMapping>(ThesisMapping.class);
        return util.exportExcel(list, "mapping");
    }

    /**
     * 新增学生选题
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存学生选题
     */
    @RequiresPermissions("thesis:mapping:add")
    @Log(title = "学生选题", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ThesisMapping thesisMapping)
    {
        return toAjax(thesisMappingService.insertThesisMapping(thesisMapping));
    }

    /**
     * 修改学生选题
     */
    @GetMapping("/edit/{tmId}")
    public String edit(@PathVariable("tmId") Long tmId, ModelMap mmap)
    {
        ThesisMapping thesisMapping = thesisMappingService.selectThesisMappingById(tmId);
        mmap.put("thesisMapping", thesisMapping);
        return prefix + "/edit";
    }

    /**
     * 修改保存学生选题
     */
    @RequiresPermissions("thesis:mapping:edit")
    @Log(title = "学生选题", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ThesisMapping thesisMapping)
    {
        return toAjax(thesisMappingService.updateThesisMapping(thesisMapping));
    }

    /**
     * 删除学生选题
     */
    @RequiresPermissions("thesis:mapping:remove")
    @Log(title = "学生选题", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(thesisMappingService.deleteThesisMappingByIds(ids));
    }
}
