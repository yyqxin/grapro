package com.ruoyi.web.controller.thesis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.service.ISysUserService;
import com.ruoyi.thesis.domain.ThesisTitle;
import com.ruoyi.thesis.service.IThesisTitleService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.thesis.domain.ThesisMappingTmp;
import com.ruoyi.thesis.service.IThesisMappingTmpService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 学生选题临时Controller
 * 
 * @author grapro
 * @date 2020-08-17
 */
@Controller
@RequestMapping("/thesis/tmp")
public class ThesisMappingTmpController extends BaseController
{
    private String prefix = "thesis/tmp";

    @Autowired
    private IThesisMappingTmpService thesisMappingTmpService;

    @Autowired
    private IThesisTitleService thesisTitleService;

    @Autowired
    private ISysUserService sysUserService;

    @RequiresPermissions("thesis:tmp:view")
    @GetMapping()
    public String tmp()
    {
        return prefix + "/tmp";
    }

    /**
     * 查询学生选题临时列表
     */
    @RequiresPermissions("thesis:tmp:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ThesisMappingTmp thesisMappingTmp)
    {
        startPage();
        // 学生自建题目需要限制学生只看到自己的题目，教师能看到自己论文的
        SysUser currentUser = ShiroUtils.getSysUser();
        String roleKeys = currentUser.getRoleKeys();
        if (roleKeys.contains("student")){
            Map<String,Object> map = new HashMap<>();
            map.put("dsf"," and t.student_id = '"+ShiroUtils.getUserId()+"' ");
            thesisMappingTmp.setParams(map);
        }else if (roleKeys.contains("teacher")){
            Map<String,Object> map = new HashMap<>();
            map.put("dsf"," and t.teacher_id = '"+ShiroUtils.getUserId()+"' ");
            thesisMappingTmp.setParams(map);
        }
        List<ThesisMappingTmp> list = thesisMappingTmpService.selectThesisMappingTmpList(thesisMappingTmp);
        return getDataTable(list);
    }

    /**
     * 导出学生选题临时列表
     */
    @RequiresPermissions("thesis:tmp:export")
    @Log(title = "学生选题临时", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ThesisMappingTmp thesisMappingTmp)
    {
        List<ThesisMappingTmp> list = thesisMappingTmpService.selectThesisMappingTmpList(thesisMappingTmp);
        ExcelUtil<ThesisMappingTmp> util = new ExcelUtil<ThesisMappingTmp>(ThesisMappingTmp.class);
        return util.exportExcel(list, "tmp");
    }

    /**
     * 新增学生选题临时
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存学生选题临时
     */
    @RequiresPermissions("thesis:tmp:add")
    @Log(title = "学生选题临时", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ThesisMappingTmp thesisMappingTmp)
    {
        return toAjax(thesisMappingTmpService.insertThesisMappingTmp(thesisMappingTmp));
    }

    /**
     * 修改学生选题临时
     */
    @GetMapping("/edit/{tmTempId}")
    public String edit(@PathVariable("tmTempId") Long tmTempId, ModelMap mmap)
    {
        ThesisMappingTmp thesisMappingTmp = thesisMappingTmpService.selectThesisMappingTmpById(tmTempId);
        mmap.put("thesisMappingTmp", thesisMappingTmp);
        return prefix + "/edit";
    }

    /**
     * 修改保存学生选题临时
     */
    @RequiresPermissions("thesis:tmp:edit")
    @Log(title = "学生选题临时", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ThesisMappingTmp thesisMappingTmp)
    {
        return toAjax(thesisMappingTmpService.updateThesisMappingTmp(thesisMappingTmp));
    }

    /**
     * 修改保存学生选题临时
     */
    @RequiresPermissions("thesis:tmp:select")
    @Log(title = "学生选题临时", businessType = BusinessType.UPDATE)
    @PostMapping("/chooseOperate")
    @ResponseBody
    public AjaxResult chooseOperate(ThesisMappingTmp thesisMappingTmp)
    {
        ThesisTitle thesisTitle = thesisTitleService.selectThesisTitleById(thesisMappingTmp.getThesisId());
        if ("0".equals(thesisTitle.getStatus())){
            int studentNum=sysUserService.selectUserById(thesisTitle.getThesisTeacher()).getStudentNum();
            //判断导师指导人数是否超过6人
            if(studentNum+1>6){
                return AjaxResult.error("您已指导6名学生！");
            }else {
                return toAjax(thesisMappingTmpService.chooseOperate(thesisMappingTmp));
            }
        }
        else {
            return AjaxResult.error("论文题目已被确定，请点击拒绝");
        }
    }

    /**
     * 删除学生选题临时
     */
    @RequiresPermissions("thesis:tmp:remove")
    @Log(title = "学生选题临时", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(thesisMappingTmpService.deleteThesisMappingTmpByIds(ids));
    }

}
