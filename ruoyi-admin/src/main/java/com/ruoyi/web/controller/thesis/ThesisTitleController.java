package com.ruoyi.web.controller.thesis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.framework.web.domain.server.Sys;
import com.ruoyi.framework.web.service.ConfigService;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.service.ISysConfigService;
import com.ruoyi.system.service.ISysUserService;
import com.ruoyi.thesis.domain.ThesisMappingTmp;
import com.ruoyi.thesis.service.IThesisMappingTmpService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.thesis.domain.ThesisTitle;
import com.ruoyi.thesis.service.IThesisTitleService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 论文选题Controller
 * 
 * @author grapro
 * @date 2020-08-17
 */
@Controller
@RequestMapping("/thesis/title")
public class ThesisTitleController extends BaseController
{
    private String prefix = "thesis/title";
    @Autowired
    private ISysUserService sysUserService;
    @Autowired
    private IThesisTitleService thesisTitleService;

    @Autowired
    private IThesisMappingTmpService thesisMappingTmpService;

    @Autowired
    private ISysConfigService configService;

    @RequiresPermissions("thesis:title:view")
    @GetMapping()
    public String title()
    {
        return prefix + "/title";
    }

    /**
     * 查询导师论文选题列表
     * 教师 自身 学生 全部 管理者全部
     */
    @RequiresPermissions("thesis:title:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ThesisTitle thesisTitle)
    {
        if(StringUtils.isEmpty(thesisTitle.getThesisType())) {
            thesisTitle.setThesisType("1");
        }
        startPage();
        // 单纯教师身份只能看自己的选题
        SysUser currentUser = ShiroUtils.getSysUser();
        if (currentUser.getRoleKeys().equals("teacher")){
            Map<String,Object> map = new HashMap<>();
            map.put("dsf"," and t.thesis_teacher = '"+ShiroUtils.getUserId()+"' ");
            thesisTitle.setParams(map);
        }
        List<ThesisTitle> list = thesisTitleService.selectThesisTitleList(thesisTitle);
        return getDataTable(list);
    }


    @RequiresPermissions("thesis:title:view")
    @GetMapping("/stuListTitle")
    public String stuListTitle()
    {
        return prefix + "/stuListTitle";
    }

    /**
     * 查询自建论文选题列表
     */
    @RequiresPermissions("thesis:title:stuList")
    @PostMapping("/stuList")
    @ResponseBody
    public TableDataInfo stuList(ThesisTitle thesisTitle)
    {
        startPage();
        // 学生自建题目需要限制学生只看到自己的题目，教师能看到所有
        SysUser currentUser = ShiroUtils.getSysUser();
        if (currentUser.getRoleKeys().equals("student")){
            Map<String,Object> map = new HashMap<>();
            map.put("dsf"," and t.create_by = '"+ShiroUtils.getLoginName()+"' ");
            thesisTitle.setParams(map);
        }
        List<ThesisTitle> list = thesisTitleService.selectThesisTitleList(thesisTitle);
        return getDataTable(list);
    }

    /**
     * 导出论文选题列表
     */
    @RequiresPermissions("thesis:title:export")
    @Log(title = "论文选题", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ThesisTitle thesisTitle)
    {
        List<ThesisTitle> list = thesisTitleService.selectThesisTitleList(thesisTitle);
        ExcelUtil<ThesisTitle> util = new ExcelUtil<ThesisTitle>(ThesisTitle.class);
        return util.exportExcel(list, "title");
    }

    /**
     * 新增论文选题
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存论文选题
     */
    @RequiresPermissions("thesis:title:add")
    @Log(title = "论文选题", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ThesisTitle thesisTitle)
    {
        SysUser teacherUser=ShiroUtils.getSysUser();
        thesisTitle.setThesisTeacherUser(teacherUser);
        thesisTitle.setCreateBy(ShiroUtils.getLoginName());
        return toAjax(thesisTitleService.insertThesisTitle(thesisTitle));
    }

    /**
     * 学生自建论文
     */
    @GetMapping("/studentadd")
    public String studentAdd()
    {
        return prefix + "/studentadd";
    }

    /**
     * 学生自建论文保存
     */
    @RequiresPermissions("thesis:title:add")
    @Log(title = "论文选题", businessType = BusinessType.INSERT)
    @PostMapping("/studentadd")
    @ResponseBody
    public AjaxResult studentAddSave(ThesisTitle thesisTitle)
    {
        thesisTitle.setCreateBy(ShiroUtils.getLoginName());
        return toAjax(thesisTitleService.insertThesisTitle(thesisTitle));
    }

    /**
     * 选择论文选题
     */
    @RequiresPermissions("thesis:title:selectThesis")
    @Log(title = "选择论文选题", businessType = BusinessType.INSERT)
    @PostMapping("/selectThesis")
    @ResponseBody
    public AjaxResult selectThesis(ThesisMappingTmp thesisMappingTmp)
    {
        thesisMappingTmp.setCreateBy(ShiroUtils.getLoginName());
        thesisMappingTmp.setStudentId(ShiroUtils.getUserId());
        List<ThesisMappingTmp> thesisMappingTmps = thesisMappingTmpService.selectAcademicYearTmp(thesisMappingTmp);
        if (thesisMappingTmps.size() > 0 ){
            return AjaxResult.error("已存在选题，请查看已存在选题");
        }
        //获取该论文题目的指导老师 所指导学生的数量
        int studentNum=sysUserService.selectUserById(thesisMappingTmp.getTeacherId()).getStudentNum();
        //判断人数是否超过6人
        if(studentNum+1>6) {
            return AjaxResult.error("该导师所指导学生人数已满");
        }
        return toAjax(thesisMappingTmpService.insertThesisMappingTmp(thesisMappingTmp));
    }

    /**
     * 自建论文选择
     */
    @RequiresPermissions("thesis:title:teacherSelectThesis")
    @Log(title = "自建论文选择", businessType = BusinessType.INSERT)
    @PostMapping("/teacherSelectThesis")
    @ResponseBody
    public AjaxResult teacherSelectThesis(ThesisMappingTmp thesisMappingTmp)
    {
        Long userId = ShiroUtils.getUserId();
        return toAjax(thesisTitleService.teacherSelectThesis(thesisMappingTmp,userId));
    }

    /**
     * 修改论文选题
     */
    @GetMapping("/edit/{thesisId}")
    public String edit(@PathVariable("thesisId") Long thesisId, ModelMap mmap)
    {
        ThesisTitle thesisTitle = thesisTitleService.selectThesisTitleById(thesisId);
        mmap.put("thesisTitle", thesisTitle);
        return prefix + "/edit";
    }

    /**
     * 修改保存论文选题
     */
    @RequiresPermissions("thesis:title:edit")
    @Log(title = "论文选题", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ThesisTitle thesisTitle)
    {
        return toAjax(thesisTitleService.updateThesisTitle(thesisTitle));
    }

    /**
     * 删除论文选题
     */
    @RequiresPermissions("thesis:title:remove")
    @Log(title = "论文选题", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(thesisTitleService.deleteThesisTitleByIds(ids));
    }


    @RequiresPermissions("thesis:title:import")
    @PostMapping("/importData")
    @ResponseBody
    public AjaxResult importData(MultipartFile file) throws Exception
    {
        ExcelUtil<ThesisTitle> util = new ExcelUtil<>(ThesisTitle.class);
        List<ThesisTitle> thesisTitleList = util.importExcel(file.getInputStream());
        String academicYear = configService.selectConfigByKey("thesis.title.academicYear");
        String loginName = ShiroUtils.getSysUser().getLoginName();
        String message = thesisTitleService.importThesis(thesisTitleList, loginName, academicYear);
        return AjaxResult.success(message);
    }

    @RequiresPermissions("thesis:title:view")
    @GetMapping("/importTemplate")
    @ResponseBody
    public AjaxResult importTemplate()
    {
        ExcelUtil<ThesisTitle> util = new ExcelUtil<ThesisTitle>(ThesisTitle.class);
        return util.importTemplateExcel("论文题目");
    }
}
