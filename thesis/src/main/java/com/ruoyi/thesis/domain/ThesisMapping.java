package com.ruoyi.thesis.domain;

import com.ruoyi.system.domain.SysUser;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 学生选题对象 thesis_mapping
 * 
 * @author ruoyi
 * @date 2020-08-17
 */
public class ThesisMapping extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 双选确认id */
    private Long tmId;

    /** 题目id */
    @Excel(name = "题目id")
    private Long thesisId;

    /** 教师id */
    @Excel(name = "教师id")
    private Long teacherId;

    /** 学生id */
    @Excel(name = "学生id")
    private Long studentId;

    private String academicYear;

    private ThesisTitle thesisTitle;

    private SysUser teacherUser;

    private SysUser studentUser;

    /** 指导内容 */
    @Excel(name = "指导内容")
    private String guideContent;

    /** 删除标志 : 删除标志 */
    private String delFlag;

    /** 开题报考文件路径 */
    private String  report_url;

    /** 自我评价文件路径 */
    private String  selfestimate_url;

    /** 教师评价文件路径 */
    private String  teacherestimate_url;

    /** 复审评价文件路径 */
    private String  recheck_url;

    /** 答辩记录文件路径 */
    private String  defensenotes_url;

    /** 成绩评定文件路径 */
    private String  grade_url;

    /** 中期检查文件路径 */
    private String  midcheck_url;

    /** 检测报告文件路径 */
    private String  checkreport_url;

    /** 毕业论文文件路径 */
    private String  project_url;


    public void setTmId(Long tmId)
    {
        this.tmId = tmId;
    }

    public Long getTmId()
    {
        return tmId;
    }
    public void setThesisId(Long thesisId)
    {
        this.thesisId = thesisId;
    }

    public Long getThesisId()
    {
        return thesisId;
    }
    public void setTeacherId(Long teacherId)
    {
        this.teacherId = teacherId;
    }

    public Long getTeacherId()
    {
        return teacherId;
    }
    public void setStudentId(Long studentId)
    {
        this.studentId = studentId;
    }

    public Long getStudentId()
    {
        return studentId;
    }
    public void setGuideContent(String guideContent)
    {
        this.guideContent = guideContent;
    }

    public String getGuideContent()
    {
        return guideContent;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }

    public String getAcademicYear() {
        return academicYear;
    }

    public void setAcademicYear(String academicYear) {
        this.academicYear = academicYear;
    }

    public ThesisTitle getThesisTitle() {
        return thesisTitle;
    }

    public void setThesisTitle(ThesisTitle thesisTitle) {
        this.thesisTitle = thesisTitle;
    }

    public SysUser getTeacherUser() {
        return teacherUser;
    }

    public void setTeacherUser(SysUser teacherUser) {
        this.teacherUser = teacherUser;
    }

    public SysUser getStudentUser() {
        return studentUser;
    }

    public void setStudentUser(SysUser studentUser) {
        this.studentUser = studentUser;
    }

    public String getReport_url() {
        return report_url;
    }

    public void setReport_url(String report_url) {
        this.report_url = report_url;
    }

    public String getSelfestimate_url() {
        return selfestimate_url;
    }

    public void setSelfestimate_url(String selfestimate_url) {
        this.selfestimate_url = selfestimate_url;
    }

    public String getTeacherestimate_url() {
        return teacherestimate_url;
    }

    public void setTeacherestimate_url(String teacherestimate_url) {
        this.teacherestimate_url = teacherestimate_url;
    }

    public String getRecheck_url() {
        return recheck_url;
    }

    public void setRecheck_url(String recheck_url) {
        this.recheck_url = recheck_url;
    }

    public String getDefensenotes_url() {
        return defensenotes_url;
    }

    public void setDefensenotes_url(String defensenotes_url) {
        this.defensenotes_url = defensenotes_url;
    }

    public String getGrade_url() {
        return grade_url;
    }

    public void setGrade_url(String grade_url) {
        this.grade_url = grade_url;
    }

    public String getMidcheck_url() {
        return midcheck_url;
    }

    public void setMidcheck_url(String midcheck_url) {
        this.midcheck_url = midcheck_url;
    }

    public String getCheckreport_url() {
        return checkreport_url;
    }

    public void setCheckreport_url(String checkreport_url) {
        this.checkreport_url = checkreport_url;
    }

    public String getProject_url() {
        return project_url;
    }

    public void setProject_url(String project_url) {
        this.project_url = project_url;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("tmId", getTmId())
            .append("thesisId", getThesisId())
            .append("teacherId", getTeacherId())
            .append("studentId", getStudentId())
            .append("guideContent", getGuideContent())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("delFlag", getDelFlag())
            .toString();
    }
}
