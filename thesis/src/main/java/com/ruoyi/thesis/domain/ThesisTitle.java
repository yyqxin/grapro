package com.ruoyi.thesis.domain;

import com.ruoyi.common.annotation.Excels;
import com.ruoyi.system.domain.SysUser;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 论文选题对象 thesis_title
 * 
 * @author ruoyi
 * @date 2020-08-17
 */
public class ThesisTitle extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 论文id */
    private Long thesisId;

    /** 题目名称 */
    @Excel(name = "*题目名称")
    private String thesisName;

    /** 题目介绍 */
    @Excel(name = "题目介绍")
    private String thesisDesc;

    /** 题目类型 */
    @Excel(name = "*题目类型",combo = {"导师题目","自建题目"}, readConverterExp = "1=导师题目,2=自建题目",dictType = "thesis_type")
    private String thesisType;

    /** 学年 */
    @Excel(name = "*学年",type = Excel.Type.EXPORT)
    private String academicYear;

    private String status;

    /** 指导教师 */
    private Long thesisTeacher;

    @Excels({
        @Excel(name = "导师姓名",targetAttr = "userName",type = Excel.Type.EXPORT),
        @Excel(name = "*导师工号",targetAttr = "loginName", type = Excel.Type.EXPORT)
    })
    private SysUser thesisTeacherUser;

    /** 指导教师 */
    private String loginName;

    /** 创建人姓名 */
    private String createByName;

    /** 删除标志 0未删除 1已删除 */
    private String delFlag = "0";

    /**指导学生数量*/
    private int studentNum;

    public int getStudentNum() {
        return studentNum;
    }

    public void setStudentNum(int studentNum) {
        this.studentNum = studentNum;
    }

    public void setThesisId(Long thesisId)
    {
        this.thesisId = thesisId;
    }

    public Long getThesisId() 
    {
        return thesisId;
    }
    public void setThesisName(String thesisName) 
    {
        this.thesisName = thesisName;
    }

    public String getThesisName() 
    {
        return thesisName;
    }
    public void setThesisDesc(String thesisDesc) 
    {
        this.thesisDesc = thesisDesc;
    }

    public String getThesisDesc() 
    {
        return thesisDesc;
    }
    public void setThesisType(String thesisType) 
    {
        this.thesisType = thesisType;
    }

    public String getThesisType() 
    {
        return thesisType;
    }
    public void setAcademicYear(String academicYear) 
    {
        this.academicYear = academicYear;
    }

    public String getAcademicYear() 
    {
        return academicYear;
    }
    public void setThesisTeacher(Long thesisTeacher) 
    {
        this.thesisTeacher = thesisTeacher;
    }

    public Long getThesisTeacher() 
    {
        return thesisTeacher;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDelFlag()
    {
        return delFlag;
    }

    public SysUser getThesisTeacherUser() {
        return thesisTeacherUser;
    }

    public void setThesisTeacherUser(SysUser thesisTeacherUser) {
        this.thesisTeacherUser = thesisTeacherUser;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getCreateByName() {
        return createByName;
    }

    public void setCreateByName(String createByName) {
        this.createByName = createByName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("thesisId", getThesisId())
            .append("thesisName", getThesisName())
            .append("thesisDesc", getThesisDesc())
            .append("thesisType", getThesisType())
            .append("academicYear", getAcademicYear())
            .append("thesisTeacher", getThesisTeacher())
            .append("status", getStatus())
            .append("loginName", getLoginName())
            .append("createBy", getCreateBy())
            .append("createByName", getCreateByName())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("delFlag", getDelFlag())
            .toString();
    }
}
