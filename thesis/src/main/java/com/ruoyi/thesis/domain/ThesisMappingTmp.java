package com.ruoyi.thesis.domain;

import com.ruoyi.system.domain.SysUser;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 学生选题临时对象 thesis_mapping_tmp
 * 
 * @author ruoyi
 * @date 2020-08-17
 */
public class ThesisMappingTmp extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 选题id */
    private Long tmTempId;

    /** 题目id */
    @Excel(name = "题目id")
    private Long thesisId;

    private ThesisTitle thesisTitle;

    /** 教师id */
    @Excel(name = "教师id")
    private Long teacherId;

    private SysUser teacherUser;

    /** 学生id */
    @Excel(name = "学生id")
    private Long studentId;

    private SysUser studentUser;

    private String academicYear;

    /** 审批状态 : 0 待确认 1 已确认 2 已拒绝
     * 默认 0
     * */
    @Excel(name = "审批状态 : 0 待确认 1 已确认 2 已拒绝 ")
    private String status;

    /** 删除标志 : 0 未删除 1 已删除 */
    private String delFlag = "0";

    public void setTmTempId(Long tmTempId) 
    {
        this.tmTempId = tmTempId;
    }

    public Long getTmTempId() 
    {
        return tmTempId;
    }

    public Long getThesisId() {
        return thesisId;
    }

    public void setThesisId(Long thesisId) {
        this.thesisId = thesisId;
    }

    public ThesisTitle getThesisTitle() {
        return thesisTitle;
    }

    public void setThesisTitle(ThesisTitle thesisTitle) {
        this.thesisTitle = thesisTitle;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public SysUser getTeacherUser() {
        return teacherUser;
    }

    public void setTeacherUser(SysUser teacherUser) {
        this.teacherUser = teacherUser;
    }

    public SysUser getStudentUser() {
        return studentUser;
    }

    public void setStudentUser(SysUser studentUser) {
        this.studentUser = studentUser;
    }

    public String getStatus()
    {
        return status;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    public String getAcademicYear() {
        return academicYear;
    }

    public void setAcademicYear(String academicYear) {
        this.academicYear = academicYear;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("tmTempId", getTmTempId())
            .append("thesisId", getThesisId())
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("delFlag", getDelFlag())
            .toString();
    }
}
