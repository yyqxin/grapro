package com.ruoyi.thesis.mapper;

import java.util.List;
import com.ruoyi.thesis.domain.ThesisMapping;
import com.ruoyi.thesis.domain.ThesisMappingTmp;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

/**
 * 学生选题Mapper接口
 * 
 * @author ruoyi
 * @date 2020-08-17
 */
public interface ThesisMappingMapper {
    /**
     * 查询学生选题
     *
     * @param tmId 学生选题ID
     * @return 学生选题
     */
    public ThesisMapping selectThesisMappingById(Long tmId);

    /**
     * 查询学生选题列表
     *
     * @param thesisMapping 学生选题
     * @return 学生选题集合
     */
    public List<ThesisMapping> selectThesisMappingList(ThesisMapping thesisMapping);

    /**
     * 新增学生选题
     *
     * @param thesisMapping 学生选题
     * @return 结果
     */
    public int insertThesisMapping(ThesisMapping thesisMapping);

    /**
     * 修改学生选题
     *
     * @param thesisMapping 学生选题
     * @return 结果
     */
    public int updateThesisMapping(ThesisMapping thesisMapping);

    /**
     * 删除学生选题
     *
     * @param tmId 学生选题ID
     * @return 结果
     */
    public int deleteThesisMappingById(Long tmId);

    /**
     * 批量删除学生选题
     *
     * @param tmIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteThesisMappingByIds(String[] tmIds);

    int insertThesisMappingByTmp(ThesisMappingTmp thesisMappingTmp);

}
