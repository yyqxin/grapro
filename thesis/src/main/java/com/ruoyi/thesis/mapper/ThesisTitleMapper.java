package com.ruoyi.thesis.mapper;

import java.util.List;

import com.ruoyi.thesis.domain.ThesisMappingTmp;
import com.ruoyi.thesis.domain.ThesisTitle;

/**
 * 论文选题Mapper接口
 * 
 * @author ruoyi
 * @date 2020-08-17
 */
public interface ThesisTitleMapper 
{
    /**
     * 查询论文选题
     * 
     * @param thesisId 论文选题ID
     * @return 论文选题
     */
    public ThesisTitle selectThesisTitleById(Long thesisId);

    /**
     * 查询论文选题列表
     * 
     * @param thesisTitle 论文选题
     * @return 论文选题集合
     */
    public List<ThesisTitle> selectThesisTitleList(ThesisTitle thesisTitle);

    /**
     * 查询论文选题
     * 条件：名称、学年、教师工号
     * @param thesisTitle 论文选题
     * @return 论文选题集合
     */
    ThesisTitle selectThesisTitle(ThesisTitle thesisTitle);

    /**
     * 新增论文选题
     * 
     * @param thesisTitle 论文选题
     * @return 结果
     */
    public int insertThesisTitle(ThesisTitle thesisTitle);

    /**
     * 修改论文选题
     * 
     * @param thesisTitle 论文选题
     * @return 结果
     */
    public int updateThesisTitle(ThesisTitle thesisTitle);

    /**
     * 删除论文选题
     * 
     * @param thesisId 论文选题ID
     * @return 结果
     */
    public int deleteThesisTitleById(Long thesisId);

    /**
     * 批量删除论文选题
     * 
     * @param thesisIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteThesisTitleByIds(String[] thesisIds);
}
