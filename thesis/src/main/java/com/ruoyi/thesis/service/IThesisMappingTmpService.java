package com.ruoyi.thesis.service;

import java.util.List;
import com.ruoyi.thesis.domain.ThesisMappingTmp;

/**
 * 学生选题临时Service接口
 * 
 * @author ruoyi
 * @date 2020-08-17
 */
public interface IThesisMappingTmpService 
{
    /**
     * 查询学生选题临时
     * 
     * @param tmTempId 学生选题临时ID
     * @return 学生选题临时
     */
    public ThesisMappingTmp selectThesisMappingTmpById(Long tmTempId);

    /**
     * 查询学生选题临时列表
     * 
     * @param thesisMappingTmp 学生选题临时
     * @return 学生选题临时集合
     */
    public List<ThesisMappingTmp> selectThesisMappingTmpList(ThesisMappingTmp thesisMappingTmp);

    /**
     * 新增学生选题临时
     * 
     * @param thesisMappingTmp 学生选题临时
     * @return 结果
     */
    public int insertThesisMappingTmp(ThesisMappingTmp thesisMappingTmp);

    /**
     * 修改学生选题临时
     * 
     * @param thesisMappingTmp 学生选题临时
     * @return 结果
     */
    public int updateThesisMappingTmp(ThesisMappingTmp thesisMappingTmp);

    /**
     * 批量删除学生选题临时
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteThesisMappingTmpByIds(String ids);

    /**
     * 删除学生选题临时信息
     * 
     * @param tmTempId 学生选题临时ID
     * @return 结果
     */
    public int deleteThesisMappingTmpById(Long tmTempId);

    public int chooseOperate(ThesisMappingTmp thesisMappingTmp);

    List<ThesisMappingTmp> selectAcademicYearTmp(ThesisMappingTmp thesisMappingTmp);
}
