package com.ruoyi.thesis.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.thesis.mapper.ThesisMappingMapper;
import com.ruoyi.thesis.domain.ThesisMapping;
import com.ruoyi.thesis.service.IThesisMappingService;
import com.ruoyi.common.core.text.Convert;

/**
 * 学生选题Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-08-17
 */
@Service
public class ThesisMappingServiceImpl implements IThesisMappingService 
{
    @Autowired
    private ThesisMappingMapper thesisMappingMapper;

    /**
     * 查询学生选题
     * 
     * @param tmId 学生选题ID
     * @return 学生选题
     */
    @Override
    public ThesisMapping selectThesisMappingById(Long tmId)
    {
        return thesisMappingMapper.selectThesisMappingById(tmId);
    }

    /**
     * 查询学生选题列表
     * 
     * @param thesisMapping 学生选题
     * @return 学生选题
     */
    @Override
    public List<ThesisMapping> selectThesisMappingList(ThesisMapping thesisMapping)
    {
        return thesisMappingMapper.selectThesisMappingList(thesisMapping);
    }

    /**
     * 新增学生选题
     * 
     * @param thesisMapping 学生选题
     * @return 结果
     */
    @Override
    public int insertThesisMapping(ThesisMapping thesisMapping)
    {
        thesisMapping.setCreateTime(DateUtils.getNowDate());
        return thesisMappingMapper.insertThesisMapping(thesisMapping);
    }

    /**
     * 修改学生选题
     * 
     * @param thesisMapping 学生选题
     * @return 结果
     */
    @Override
    public int updateThesisMapping(ThesisMapping thesisMapping)
    {
        thesisMapping.setUpdateTime(DateUtils.getNowDate());
        return thesisMappingMapper.updateThesisMapping(thesisMapping);
    }

    /**
     * 删除学生选题对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteThesisMappingByIds(String ids)
    {
        return thesisMappingMapper.deleteThesisMappingByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除学生选题信息
     * 
     * @param tmId 学生选题ID
     * @return 结果
     */
    @Override
    public int deleteThesisMappingById(Long tmId)
    {
        return thesisMappingMapper.deleteThesisMappingById(tmId);
    }
}
