package com.ruoyi.thesis.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.service.ISysUserService;
import com.ruoyi.thesis.domain.ThesisMapping;
import com.ruoyi.thesis.domain.ThesisTitle;
import com.ruoyi.thesis.mapper.ThesisMappingMapper;
import com.ruoyi.thesis.mapper.ThesisTitleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.thesis.mapper.ThesisMappingTmpMapper;
import com.ruoyi.thesis.domain.ThesisMappingTmp;
import com.ruoyi.thesis.service.IThesisMappingTmpService;
import com.ruoyi.common.core.text.Convert;

/**
 * 学生选题临时Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-08-17
 */
@Service
public class ThesisMappingTmpServiceImpl implements IThesisMappingTmpService 
{
    @Autowired
    private ThesisMappingTmpMapper thesisMappingTmpMapper;

    @Autowired
    private ThesisMappingMapper thesisMappingMapper;

    @Autowired
    private ThesisTitleMapper thesisTitleMapper;

    @Autowired
    private ISysUserService sysUserService;
    /**
     * 查询学生选题临时
     * 
     * @param tmTempId 学生选题临时ID
     * @return 学生选题临时
     */
    @Override
    public ThesisMappingTmp selectThesisMappingTmpById(Long tmTempId)
    {
        return thesisMappingTmpMapper.selectThesisMappingTmpById(tmTempId);
    }

    /**
     * 查询学生选题临时列表
     * 
     * @param thesisMappingTmp 学生选题临时
     * @return 学生选题临时
     */
    @Override
    public List<ThesisMappingTmp> selectThesisMappingTmpList(ThesisMappingTmp thesisMappingTmp)
    {
        return thesisMappingTmpMapper.selectThesisMappingTmpList(thesisMappingTmp);
    }

    /**
     * 新增学生选题临时
     * 
     * @param thesisMappingTmp 学生选题临时
     * @return 结果
     */
    @Override
    public int insertThesisMappingTmp(ThesisMappingTmp thesisMappingTmp)
    {
        thesisMappingTmp.setCreateTime(DateUtils.getNowDate());
        return thesisMappingTmpMapper.insertThesisMappingTmp(thesisMappingTmp);
    }

    /**
     * 修改学生选题临时
     * 
     * @param thesisMappingTmp 学生选题临时
     * @return 结果
     */
    @Override
    public int updateThesisMappingTmp(ThesisMappingTmp thesisMappingTmp)
    {
        thesisMappingTmp.setUpdateTime(DateUtils.getNowDate());
        return thesisMappingTmpMapper.updateThesisMappingTmp(thesisMappingTmp);
    }

    /**
     * 删除学生选题临时对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteThesisMappingTmpByIds(String ids)
    {
        return thesisMappingTmpMapper.deleteThesisMappingTmpByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除学生选题临时信息
     * 
     * @param tmTempId 学生选题临时ID
     * @return 结果
     */
    @Override
    public int deleteThesisMappingTmpById(Long tmTempId)
    {
        return thesisMappingTmpMapper.deleteThesisMappingTmpById(tmTempId);
    }

    @Override
    public int chooseOperate(ThesisMappingTmp thesisMappingTmp) {
        String status = thesisMappingTmp.getStatus();
        if (StringUtils.isNotEmpty(status) && status.equals("1")){
            // 更新原来论文为不可选
            ThesisTitle thesisTitle = thesisTitleMapper.selectThesisTitleById(thesisMappingTmp.getThesisId());
            thesisTitle.setStatus("1");
            thesisTitleMapper.updateThesisTitle(thesisTitle);
            // 同意选题
            ThesisMappingTmp temp = thesisMappingTmpMapper.selectThesisMappingTmpById(thesisMappingTmp.getTmTempId());
            thesisMappingMapper.insertThesisMappingByTmp(temp);
            //导师指导学生人数加1
            int studentNum=sysUserService.selectUserById(thesisTitle.getThesisTeacher()).getStudentNum() ;
            SysUser teacher=sysUserService.selectUserById(thesisTitle.getThesisTeacher());
            teacher.setStudentNum(studentNum+1);
            sysUserService.updateUserInfo(teacher);
            return thesisMappingTmpMapper.updateThesisMappingTmp(thesisMappingTmp);
        }else {
            // 拒绝选题
            return thesisMappingTmpMapper.updateThesisMappingTmp(thesisMappingTmp);
        }
    }

    @Override
    public List<ThesisMappingTmp> selectAcademicYearTmp(ThesisMappingTmp thesisMappingTmp) {
        return thesisMappingTmpMapper.selectStudentAcademicYearTmp(thesisMappingTmp);
    }
}
