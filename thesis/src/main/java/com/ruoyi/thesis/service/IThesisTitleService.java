package com.ruoyi.thesis.service;

import java.util.List;

import com.ruoyi.thesis.domain.ThesisMappingTmp;
import com.ruoyi.thesis.domain.ThesisTitle;

/**
 * 论文选题Service接口
 * 
 * @author ruoyi
 * @date 2020-08-17
 */
public interface IThesisTitleService 
{
    /**
     * 查询论文选题
     * 
     * @param thesisId 论文选题ID
     * @return 论文选题
     */
    public ThesisTitle selectThesisTitleById(Long thesisId);

    /**
     * 查询论文选题列表
     * 
     * @param thesisTitle 论文选题
     * @return 论文选题集合
     */
    public List<ThesisTitle> selectThesisTitleList(ThesisTitle thesisTitle);

    /**
     * 新增论文选题
     * 
     * @param thesisTitle 论文选题
     * @return 结果
     */
    public int insertThesisTitle(ThesisTitle thesisTitle);

    /**
     * 修改论文选题
     * 
     * @param thesisTitle 论文选题
     * @return 结果
     */
    public int updateThesisTitle(ThesisTitle thesisTitle);

    /**
     * 批量删除论文选题
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteThesisTitleByIds(String ids);

    /**
     * 删除论文选题信息
     * 
     * @param thesisId 论文选题ID
     * @return 结果
     */
    public int deleteThesisTitleById(Long thesisId);

    /**
     *  教师导入论文题目
     * @param thesisTitleList 导入集合
     * @param updateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param loginName 操作人、当前导师账号
     * @param academicYear 当前学年
     * @return
     */
    String importThesis(List<ThesisTitle> thesisTitleList, String loginName,String academicYear);

    int teacherSelectThesis(ThesisMappingTmp thesisMappingTmp,Long userId);
}
