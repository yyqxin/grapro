package com.ruoyi.thesis.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.mapper.SysUserMapper;
import com.ruoyi.thesis.domain.ThesisMapping;
import com.ruoyi.thesis.domain.ThesisMappingTmp;
import com.ruoyi.thesis.mapper.ThesisMappingMapper;
import com.ruoyi.thesis.mapper.ThesisMappingTmpMapper;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.thesis.mapper.ThesisTitleMapper;
import com.ruoyi.thesis.domain.ThesisTitle;
import com.ruoyi.thesis.service.IThesisTitleService;
import com.ruoyi.common.core.text.Convert;

/**
 * 论文选题Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-08-17
 */
@Service
public class ThesisTitleServiceImpl implements IThesisTitleService 
{
    @Autowired
    private ThesisTitleMapper thesisTitleMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private ThesisMappingTmpMapper thesisMappingTmpMapper;

    @Autowired
    private ThesisMappingMapper thesisMappingMapper;

    /**
     * 查询论文选题
     * 
     * @param thesisId 论文选题ID
     * @return 论文选题
     */
    @Override
    public ThesisTitle selectThesisTitleById(Long thesisId)
    {
        return thesisTitleMapper.selectThesisTitleById(thesisId);
    }

    /**
     * 查询论文选题列表
     * 
     * @param thesisTitle 论文选题
     * @return 论文选题
     */
    @Override
    public List<ThesisTitle> selectThesisTitleList(ThesisTitle thesisTitle)
    {
        return thesisTitleMapper.selectThesisTitleList(thesisTitle);
    }

    /**
     * 新增论文选题
     * 
     * @param thesisTitle 论文选题
     * @return 结果
     */
    @Override
    public int insertThesisTitle(ThesisTitle thesisTitle)
    {
        thesisTitle.setCreateTime(DateUtils.getNowDate());
        return thesisTitleMapper.insertThesisTitle(thesisTitle);
    }

    /**
     * 修改论文选题
     * 
     * @param thesisTitle 论文选题
     * @return 结果
     */
    @Override
    public int updateThesisTitle(ThesisTitle thesisTitle)
    {
        thesisTitle.setUpdateTime(DateUtils.getNowDate());
        return thesisTitleMapper.updateThesisTitle(thesisTitle);
    }

    /**
     * 删除论文选题对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteThesisTitleByIds(String ids)
    {
        return thesisTitleMapper.deleteThesisTitleByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除论文选题信息
     * 
     * @param thesisId 论文选题ID
     * @return 结果
     */
    @Override
    public int deleteThesisTitleById(Long thesisId)
    {
        return thesisTitleMapper.deleteThesisTitleById(thesisId);
    }

    @Override
    public String importThesis(List<ThesisTitle> thesisTitleList, String loginName,String academicYear) {
        // 导入的校验 名称 不能为空
        List<ThesisTitle> errorList = thesisTitleList.stream().filter(thesisTitle -> {
            if ("".equals(thesisTitle.getThesisName())){
                return true;
            }
            return false;
        }).collect(Collectors.toList());
        if (CollectionUtils.isNotEmpty(errorList)){
            return "存在"+errorList.size()+"条题目数据名称为空";
        }
        // 同名称、同学年的做更新，否则做新增
        for (ThesisTitle title : thesisTitleList){
            title.setAcademicYear(academicYear);
            // 导入时导师User 要手动设置，导入无法反射该对象
            SysUser teacher = new SysUser();
            teacher.setLoginName(loginName);
            title.setThesisTeacherUser(teacher);
            // 根据 名称、学年、工号 作为条件查询
            ThesisTitle temp = thesisTitleMapper.selectThesisTitle(title);
            if (temp != null){
                // 已存在进行数据更新
                title.setThesisId(temp.getThesisId());
                thesisTitleMapper.updateThesisTitle(title);
            }else {
                // 题目类型、创建人 信息补充
                title.setThesisType("1");
                title.setCreateTime(DateUtils.getNowDate());
                title.setCreateBy(loginName);
                thesisTitleMapper.insertThesisTitle(title);
            }
        }
        return "导入成功";
    }

    @Override
    public int teacherSelectThesis(ThesisMappingTmp thesisMappingTmp,Long teacherUserId) {
        SysUser teacher = sysUserMapper.selectUserById(teacherUserId);
        // 更新教师，论文题目状态
        ThesisTitle thesisTitle = thesisTitleMapper.selectThesisTitleById(thesisMappingTmp.getThesisId());
        thesisTitle.setThesisTeacherUser(teacher);
        thesisTitle.setStatus("1");
        thesisTitleMapper.updateThesisTitle(thesisTitle);

        // 创建申请
        thesisMappingTmp.setStatus("1");
        thesisMappingTmp.setTeacherId(teacherUserId);
        thesisMappingTmp.setCreateBy(teacher.getLoginName());
        SysUser student = sysUserMapper.selectUserByLoginName(thesisTitle.getCreateBy());
        thesisMappingTmp.setStudentId(student.getUserId());
        thesisMappingTmpMapper.insertThesisMappingTmp(thesisMappingTmp);

        // 创建选题
        return thesisMappingMapper.insertThesisMappingByTmp(thesisMappingTmp);

    }
}
